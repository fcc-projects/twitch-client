import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';


@Injectable()
export class TwitchService {

  constructor(private json: Jsonp) { }

  getStreamer = (x) => {
    const params = new URLSearchParams();
    params.set('callback', 'JSONP_CALLBACK');
    const url: string = 'https://wind-bow.gomix.me/twitch-api/users/' + x;
    return this.json.get(url, { search: params }).map(r => r.json());
  }
  
  getStream = (x) => {
    const params = new URLSearchParams();
    params.set('callback', 'JSONP_CALLBACK');
    const url: string = 'https://wind-bow.gomix.me/twitch-api/streams/' + x;
    return this.json.get(url, { search: params }).map(r => r.json());
  }
}